export function goToSignIn() {
    window.location.href = "/signin";
}

export function goToSignUp() {
    window.location.href = "/signup";
}

export function goToHome() {
    window.location.href = "/";
}

export function goToSignOut() {
    window.location.href = "/signout";
}

export function goToSuccess() {
    window.location.href = "/success";
}
export function goToProfile(username) {
    window.location.href = "/profile/"+ username;
}

export function goToPost(id) {
    window.location.href = `/post?id=${id}`;
}

export function goToAddPost(forkedPost = null) {

    window.location.href = forkedPost !== null ? `/addpost?forkedPost=${forkedPost}` : '/addpost';
}


