import React, {useState, useRef} from "react";

import './PostPage.css';
import {useSearchParams} from "react-router-dom";
import axios from "axios";
import {API_URL} from "../../services/http-request.service";
import Card from "@mui/material/Card";
import {Avatar, Backdrop, Box, CardActions, CardContent, CardHeader, Collapse, Modal, Typography} from "@mui/material";
import IconButton from "@mui/material/IconButton";
import Button from "@material-ui/core/Button";
import {goToAddPost} from "../../services/routing.service";
import Navbar_code from "../navbar/Navbar-code";
import Editor from "@monaco-editor/react";
import Axios from "axios";


const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 600,
    bgcolor: ' #E5ECF8',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
};

const PostPage = () => {
    const [searchParams] = useSearchParams();
    const id = searchParams.get("id");
    const user = window.localStorage.getItem("currentUser");
    const token = JSON.parse(user).accessToken;
    const [language, setUserLanguage] = useState("");
    const [caption, setCaption] = useState("");
    const [userLang, setUserLang] = useState("Python");
    const [username, setUsername] = useState(JSON.parse(user).username)
    const [userInput, setUserInput] = useState("");
    const [userOutput, setUserOutput] = useState("");
    const [userCode, setUserCode] = useState("");
    const [code, setCode] = useState("");
    const editRef = useRef(null);
    const [open, setOpen] = React.useState(false);
    const handleClose = () => setOpen(false);


    function compile() {

        if (userCode === ``) {
            return
        }

        Axios.post(`https://serverpa.herokuapp.com/compile`, {
            code: userCode,
            language: userLang,
            input: userInput
        }).then((res) => {
            if (res.data.success) {
                setUserOutput(res.data.output);
                setOpen(true);
            } else {
                setUserOutput(res.data.error);

            }
        }).catch((error) => {
            console.log(error)
        })

    }

    function clearOutput() {
        setUserOutput("");
    }

    const fetchPost = async () => {
        const postResponse = await axios.get(API_URL + '/api/posts/' + id);
        setCode(postResponse.data.code);
        setUserCode(postResponse.data.code);
        setUserLanguage(postResponse.data.language);
        setCaption(postResponse.data.caption);
    }

    fetchPost().then();

    return (
        <div className="post-page">
            <Card sx={{marginTop: '20px'}}>
                <CardContent>
                    <IconButton aria-label="fork_post">
                        <Button onClick={() => goToAddPost(id)}> Fork </Button>
                    </IconButton>
                    <Typography variant="body2" color="text.secondary">
                        {caption}
                    </Typography>
                </CardContent>
                <div className="App">
                    <Navbar_code
                        userLang={userLang} setUserLang={setUserLang}

                    />

                    <div className="main">
                        <div className="left-container">
                            <Editor

                                padding={15}
                                style={{

                                    fontSize: 12,
                                    backgroundColor: "#000000",
                                    fontFamily: 'ui-monospace,SFMono-Regular,SF Mono,Consolas,Liberation Mono,Menlo,monospace',
                                }}
                                theme="vs-dark"
                                language={language ? (language) : (userLang)}
                                defaultLanguage={language}

                                value={code ? (code) : ("# Enter Your Code Here ...")}
                                onChange={(value) => {
                                    setUserCode(value)
                                }}
                            />
                            <button className="run-btn" onClick={() => compile()}>
                                Run
                            </button>

                            <div className="right-container">
                                <h4 className="text-inp">Input:</h4>
                                <div className="input-box">
                                <textarea id="code-inp" onChange=
                                    {(e) => setUserInput(e.target.value)}>
                                </textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <Modal
                    onClose={handleClose}
                    open={open}
                    closeAfterTransition
                    BackdropComponent={Backdrop}
                    BackdropProps={{
                        timeout: 500,
                    }}
                >
                    <Box sx={style}>
                        <pre
                            style={{minHeight: "200px", backgroundColor: " #E5ECF8", color: "black"}}>{userOutput}</pre>
                        <button onClick={() => {
                            clearOutput()
                        }}
                                className="clear-btn">
                            Clear
                        </button>
                    </Box>
                </Modal>

            </Card>
        </div>
    );
};

export default PostPage;